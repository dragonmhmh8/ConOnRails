# frozen_string_literal: true

class DutyBoardPolicy < ApplicationPolicy
  def index?
    true
  end
end
